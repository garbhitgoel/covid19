package org.kmsg.cv.model;

import java.util.List;

public class CVStatsHistoryList 
{
	private List<CVStatsHistory> data;

	public List<CVStatsHistory> getData() {
		return data;
	}

	public void setData(List<CVStatsHistory> data) {
		this.data = data;
	}
}
