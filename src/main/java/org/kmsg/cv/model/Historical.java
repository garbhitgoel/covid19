package org.kmsg.cv.model;

public class Historical 
{
	private String dateRep;
	private String countriesAndTerritories;
	private String countryterritoryCode;
	private String popData2018;
	private String deaths;
	private String geoId;
	private String day;
	private String month;
	private String cases;
	private String year;
	
	public String getDateRep() {
		return dateRep;
	}
	public void setDateRep(String dateRep) {
		this.dateRep = dateRep;
	}
	public String getCountriesAndTerritories() {
		return countriesAndTerritories;
	}
	public void setCountriesAndTerritories(String countriesAndTerritories) {
		this.countriesAndTerritories = countriesAndTerritories;
	}
	public String getCountryterritoryCode() {
		return countryterritoryCode;
	}
	public void setCountryterritoryCode(String countryterritoryCode) {
		this.countryterritoryCode = countryterritoryCode;
	}
	public String getPopData2018() {
		return popData2018;
	}
	public void setPopData2018(String popData2018) {
		this.popData2018 = popData2018;
	}
	public String getDeaths() {
		return deaths;
	}
	public void setDeaths(String deaths) {
		this.deaths = deaths;
	}
	public String getGeoId() {
		return geoId;
	}
	public void setGeoId(String geoId) {
		this.geoId = geoId;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getCases() {
		return cases;
	}
	public void setCases(String cases) {
		this.cases = cases;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
}
