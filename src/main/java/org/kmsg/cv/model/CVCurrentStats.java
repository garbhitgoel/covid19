package org.kmsg.cv.model;

public class CVCurrentStats 
{
	private int totalCases;
	private int totalDeaths;
	private String maxTotalCases;
	private int todayCases;
	private int todayDeaths;
	private String maxTotalDeaths;
	private int totalRecovery;
	
	public int getTotalCases() {
		return totalCases;
	}
	public void setTotalCases(int totalCases) {
		this.totalCases = totalCases;
	}
	public int getTotalDeaths() {
		return totalDeaths;
	}
	public void setTotalDeaths(int totalDeaths) {
		this.totalDeaths = totalDeaths;
	}
	public String getMaxTotalCases() {
		return maxTotalCases;
	}
	public void setMaxTotalCases(String maxTotalCases) {
		this.maxTotalCases = maxTotalCases;
	}
	public int getTodayCases() {
		return todayCases;
	}
	public void setTodayCases(int todayCases) {
		this.todayCases = todayCases;
	}
	public int getTodayDeaths() {
		return todayDeaths;
	}
	public void setTodayDeaths(int todayDeaths) {
		this.todayDeaths = todayDeaths;
	}
	public String getMaxTotalDeaths() {
		return maxTotalDeaths;
	}
	public void setMaxTotalDeaths(String maxTotalDeaths) {
		this.maxTotalDeaths = maxTotalDeaths;
	}
	public int getTotalRecovery() {
		return totalRecovery;
	}
	public void setTotalRecovery(int totalRecovery) {
		this.totalRecovery = totalRecovery;
	}
	

}
